<?php

namespace Drupal\learnfailing_failure\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form alter implementation for failure node form.
 */
class FailureNodeAddFormAlter implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['hook_event_dispatcher.form.alter'] = ['formAlter'];

    return $events;
  }

  /**
   * Reacts on hook_event_dispatcher.form.alter event dispatchment.
   */
  public function formAlter(FormAlterEvent $event) {
    $forms = [
      'node_failure_form',
      'node_failure_edit_form',
    ];
    if (in_array($event->getFormId(), $forms)) {
      $form = &$event->getForm();
      $form['#title'] = $this->t('Tell us your failure so that we all can learn');
      $form['field_bricks']['widget']['form']['inline_entity_form']['#process'][] = [
        self::class,
        'alterFailureFormBricksForm',
      ];
      $form['actions']['submit']['#value'] = $this->t('Submit');
    }
  }

  /**
   * Process callback: Alter bricks form in failure node add form.
   *
   * Change field text label and hide actions.
   */
  public static function alterFailureFormBricksForm($element) {
    $element['field_text']['widget'][0]['#title'] = t('Give details about your failure:');
    $element['actions']['#access'] = FALSE;
    return $element;
  }

}
