<?php

namespace LearnFailing;

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;

/**
 * Context to delete the entities created in scenarios.
 */
class EntityDeleteContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * Entity types we want to delete after a scenario.
   *
   * @var array
   */
  protected $entityTypes = [
    'node',
    'user',
  ];

  /**
   * List of last entity ids grouped by entity type.
   *
   * @var array
   */
  protected $lastEntityIdByType = [];

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Initializes the context.
   *
   * @param array $parameters
   *   Context parameters. The most important is entity_types, where it
   *   can be set the additional list of entity types, that will be used
   *   to delete the new entities generated during tests.
   */
  public function __construct(array $parameters = []) {
    $this->entityTypes = array_merge($this->entityTypes, $parameters['entity_types']);
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * For each entity type store the last entity id.
   *
   * @BeforeScenario
   */
  public function storeLastEntityId() {
    foreach ($this->entityTypes as $entity_type) {
      $this->lastEntityIdByType[$entity_type] = $this->getLastEntityId($entity_type);
    }
  }

  /**
   * Get last entity id.
   *
   * Use \Drupal::entityQuery for it.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return int
   *   Last entity id, 0 if we didn't found results.
   */
  public function getLastEntityId($entity_type) {
    $entity_type_def = $this->entityTypeManager->getDefinition($entity_type);
    $query = \Drupal::entityQuery($entity_type);
    $query->sort($entity_type_def->getKey('id'), 'DESC');
    $query->range(0, 1);
    $result = $query->execute();
    return !empty($result) ? reset($result) : 0;
  }

  /**
   * Given a id to start from get entities that were created after.
   *
   * @param string $entity_type
   *   Entity type.
   * @param int $last_entity_id
   *   Last entity id of that type.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   List of new entities.
   */
  public function getNewEntities($entity_type, $last_entity_id) {
    $query = \Drupal::entityQuery($entity_type);
    $entity_type_def = $this->entityTypeManager->getDefinition($entity_type);
    $query->condition($entity_type_def->getKey('id'), $last_entity_id, '>');
    $entity_list = $query->execute();
    return !empty($entity_list) ? $this->entityTypeManager->getStorage($entity_type)->loadMultiple($entity_list) : [];
  }

  /**
   * Delete the new entities that were created in this scenario.
   *
   * @AfterScenario
   */
  public function deleteNewEntities() {
    foreach ($this->lastEntityIdByType as $entity_type => $last_entity_id) {
      foreach ($this->getNewEntities($entity_type, $last_entity_id) as $new_entity) {
        $new_entity->delete();
      }
    }
  }

}
