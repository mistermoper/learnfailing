<?php

namespace LearnFailing;

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Drupal\paragraphs\Entity\Paragraph;
use Behat\Gherkin\Node\TableNode;
use Drupal\node\Entity\Node;

/**
 * Steps related with failures behaviour.
 */
class FailureContext extends RawDrupalContext implements SnippetAcceptingContext {
  protected $failures = [];
  protected $failuresParagraphs = [];

  /**
   * Create programatically failures with title and details.
   *
   * @Given failures:
   */
  public function failures(TableNode $failures) {
    foreach ($failures->getHash() as $failure) {
      $paragraph = Paragraph::create([
        'type'       => 'text',
        'field_text' => $failure['details'],
      ]);
      $paragraph->save();
      $this->failuresParagraphs[] = $paragraph->id();
      $node = Node::create([
        'type' => 'failure',
        'title' => $failure['title'],
        'field_bricks' => [
          'depth' => NULL,
          'options' => NULL,
          'target_id' => $paragraph->id(),
        ],
      ]);
      $node->save();
      $this->failures[] = $node->id();
    }
  }

  /**
   * Delete created paragraphs / failures.
   *
   * @AfterScenario
   */
  public function deleteFailures() {
    foreach ($this->failures as $failure_nid) {
      $nid = Node::load($failure_nid);
      $nid->delete();
    }
    foreach ($this->failuresParagraphs as $paragraph_id) {
      $paragraph = Paragraph::load($paragraph_id);
      $paragraph->delete();
    }
  }

}
