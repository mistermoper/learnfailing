Feature: Failure post
  As an anonymous user
  I want post my failure
  So that anyone can learn from it.

  Variants:
  S01 - User post a failure from frontpage.
  S02 - User does not fill anything, he will see the errors.

  @api @anonymous @failure @post @create @javascript @sunnyday
  Scenario: S01 - User post a failure from frontpage, then he will see the failure created.

  Given I am an anonymous user
  When I go to "/"
  Then I should see "Post your own failure!"

  When I click "Post your own failure!"
  Then the url should match "/new-failure"
  And I should see "Tell us your failure so that we all can learn"

  Then I fill in "Title" with "I've sent a mail to the wrong client"
  Then I fill in CKEditor on field "Give details about your failure:" with "I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp."

  When I press "Submit"
  Then I should see "Failure I've sent a mail to the wrong client has been created"
  Then the url should match "/failure/ive-sent-mail-wrong-client"
  And I should see "I've sent a mail to the wrong client"
  And I should see "I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp."


  @api @anonymous @failure @post @error
  Scenario: S02 - User go to frontpage. He will be able to submit a failure.
  Given I am an anonymous user
  And I go to "/"
  And I click "Post your own failure!"

  When I press "Submit"
  Then I should not see "has been created"
  And I should see the following error message:
  | error messages                                      |
  | Title field is required.                            |
  | Give details about your failure: field is required. |
