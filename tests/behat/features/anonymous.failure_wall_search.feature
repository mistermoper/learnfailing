Feature: Failure list view
  As an anonymous user
  I want to search failures
  So that I can find the failure I want learn of

  @api @anonymous @failure @view @search
  Scenario: S01 - User will go to failures page and filter by title.

  Given failures:
  | title                                                      | details                                                                           |
  | I've sent a mail to the wrong client                       | I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp. |
  | I've tried to solve a problem alone instead of teamplaying | I've made CRM integration alone instead with my entire team.                      |

  And I am an anonymous user

  And I go to "/failures"

  Then I fill in "Title" with "team"
  When I press "Apply"
  Then I should see "I've tried to solve a problem alone instead of teamplaying"
  And I should not see "I've sent a mail to the wrong client"
