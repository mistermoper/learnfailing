Feature: Failure list view
  As an anonymous user
  I want to see the failure wall view
  So that I can learn from other's mistakes.

  @api @anonymous @failure @view @sunnyday
  Scenario: S01 -User will go to the frontpage and will see the failures user has created.
    · S02 - User will go to the frontpage and will see the failures user has created.

  Given failures:
  | title                                                      | details                                                                           |
  | I've sent a mail to the wrong client                       | I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp. |
  | I've tried to solve a problem alone instead of teamplaying | I've made CRM integration alone instead with my entire team.                      |

  And I am an anonymous user

  When I go to "/"

  Then I should see "I've sent a mail to the wrong client"
  And I should see "I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp."
  And I should see "I've tried to solve a problem alone instead of teamplaying"
  And I should see "I've made CRM integration alone instead with my entire team."

  @api @anonymous @failure @view @full
  Scenario: S02 - User will go to failures page and will see the failures user has created.

  Given failures:
  | title                                                      | details                                                                           |
  | I've sent a mail to the wrong client                       | I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp. |
  | I've tried to solve a problem alone instead of teamplaying | I've made CRM integration alone instead with my entire team.                      |

  And I am an anonymous user

  When I go to "/failures"

  Then I should see "I've sent a mail to the wrong client"
  And I should see "I wanted to send a mail to Albert from CoffeCorp but sent to Albert from TeaCorp."
  And I should see "I've tried to solve a problem alone instead of teamplaying"
  And I should see "I've made CRM integration alone instead with my entire team."
