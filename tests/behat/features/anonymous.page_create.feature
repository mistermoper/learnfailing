Feature: As an administrator
  I want to create pages
  to promote failure posting

Variants:
  · S01 - A page is created with a title.
  · S02 - A page is created with title and text field.
  · S03 - A page is created with title and block field.
  · S04 - A page is created with title and more than one paragraphs.

  @api @javascript @administrator @page @create @sunnyday
  Scenario: S01 - A page is created with a title.
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/page"
    Then I should see "Title"
    And I should see "Layout"
    Then I fill in "Title" with "Page title"
    When I press "Save"
    Then I should see "Basic page Page title has been created."
    Then I should see "Page title"

  @api @javascript @administrator @page @create @variant @text
  Scenario: S02 - A page is created with title and text field.
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/page"
    Then I should see "Title"
    And I should see "Layout"
    Then I fill in "Title" with "Page title"
    When I select "Text" from "field_bricks[actions][bundle]"
    And I press "Add new paragraph"
    And I wait for AJAX to finish at least "20" seconds
    Then I should see "Text"
    Then I fill in CKEditor on field "Text" with "Text content"

    When I press "Save"
    Then I should see "Basic page Page title has been created."
    And I should see "Page title"
    And I should see "Text content"

  @api @javascript @administrator @page @create @variant @blockfield
  Scenario: S03 - A page is created with title and block reference field.
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/page"

    Then I fill in "Title" with "Page title"

    When I select "Block" from "field_bricks[actions][bundle]"
    And I press "Add new paragraph"
    And I wait for AJAX to finish at least "20" seconds
    When I select "Failure list" from "field_bricks[form][inline_entity_form][field_block][0][plugin_id]"
    And I wait for AJAX to finish at least "20" seconds

    When I press "Save"
    Then I should see "Basic page Page title has been created."
    And I should see "Page title"
    And I should see "Failures from another persons"

  @api @javascript @administrator @page @create @variant @multiple
  Scenario: S04 - A page is created with title and more than one paragraphs.
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/page"

    Then I fill in "Title" with "Page title"

    When I select "Text" from "field_bricks[actions][bundle]"
    And I press "Add new paragraph"
    And I wait for AJAX to finish at least "20" seconds
    Then I should see "Text"
    Then I fill in CKEditor on field "Text" with "Text content"
    And I press "Create paragraph"
    And I wait for AJAX to finish at least "20" seconds

    Then I select "Block" from "field_bricks[actions][bundle]"
    And I press "Add new paragraph"
    And I wait for AJAX to finish at least "20" seconds
    Then I select "Failure list" from "field_bricks[form][inline_entity_form][field_block][0][plugin_id]"
    And I wait for AJAX to finish at least "20" seconds

    When I press "Save"
    Then I should see "Basic page Page title has been created."
    And I should see "Page title"
    And I should see "Text content"