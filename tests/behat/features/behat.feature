Feature: As a user
  I want check web is up
  So that I can enjoy it!

  @behat
  Scenario: Go to frontpage, check basic tests.
  When I go to "/"
  Then the response status code should be 200
  And I should see "Learn failing!"

  @behat @javascript
  Scenario: Go to frontpage with a real browser, check basic tests.
  When I go to "/"
  Then I should see "Learn failing!"